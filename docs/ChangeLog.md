Version 1.6.1:
* A bug with the finals tree population fixed.

Version 1.6:
* Finals changed to Olympic system.
* Tournament guide added.
* Multiple minor internal improvements and fixes.

Version 1.5:
* A duel now consists of a single fight.
* Direct printing from the tool is now available.
* Finalists count is now applied properly.
* Installer updated.

Version 1.4:
* State is correctly applied when generating finals several times in a row.
* OTM does not ask for confirmation to exit if no actions were made.
* Fixed bug in finals generation: total score is applied fully now.
