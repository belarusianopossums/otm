@ECHO OFF

REM Usage: make_installer.bat [Qt_bin_dir] [release_file.exe]
REM Update environment variables for your system
REM Resultina installer file will be placed into the .\nsis dir

IF NOT [%2] == [] (
	SET EXECUTABLE=%2
	GOTO CHECK_QTDIR
	)
SET EXECUTABLE=release\OpossumTournamentManager.exe
IF NOT EXIST %EXECUTABLE% (
	ECHO Executable file is not specified
	GOTO END
	)


:CHECK_QTDIR
IF NOT [%1] == [] (
	SET QTDIR=%1
	GOTO MAKE_INSTALLER:
	)
IF NOT "%QT_DIR%" == "" (
	SET QTDIR=%QT_DIR%
	GOTO MAKE_INSTALLER:
	)
SET QTDIR=c:\Qt\Qt5.7.1-msvc2013\5.7\msvc2013\bin
IF NOT EXIST %QTDIR%\windeployqt.exe (
	ECHO Qt dir is not specified
	GOTO END
	)

:MAKE_INSTALLER
SET INSTALLER_EXECUTABLE=installer\OpossumTournamentManager.exe
SET VCINSTALLDIR=c:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\
SET NSIS="c:\Program Files (x86)\NSIS\makensis.exe"

IF EXIST installer (
	del /S /Q installer\*.*
	)
IF NOT EXIST installer (
	mkdir installer
)
copy %EXECUTABLE% %INSTALLER_EXECUTABLE%
%QTDIR%\windeployqt.exe --release --compiler-runtime --no-translations --force %INSTALLER_EXECUTABLE%
copy docs\*.* installer\*.*

%NSIS% nsis\installer.nsi

:END