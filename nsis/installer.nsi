 
# set desktop as install directory
InstallDir $DESKTOP

  !include "MUI2.nsh"
  !include "LogicLib.nsh"
  !include "FileFunc.nsh"
  !include nsDialogs.nsh

 ;General
!define PROJECTNAME "OpossumTournamentManager"
!define VERSION "1.7"
!define FILEDIRECTORY "..\installer"
!define EXENAME "opossumtournamentmanager"
!define EXENAMENICE "OpossumTournamentManager"
!define UNINSTALLNAME "uninstall"
!define ARP "Software\Microsoft\Windows\CurrentVersion\Uninstall\OpossumTournamentManager"

OutFile "${PROJECTNAME}-${VERSION}.exe"
SetCompressor /FINAL /SOLID lzma
SetCompressorDictSize 64
InstallDir "$PROGRAMFILES\${PROJECTNAME}"

Page directory
Page instfiles
UninstPage uninstConfirm
UninstPage instfiles

Section "Install Section" SecDummy
  
  ;Get env variables - env from all users
  SetShellVarContext all
   
	;Get read only files, goes to "Program Files"
	SetOutPath "$INSTDIR"  
	File /r "${FILEDIRECTORY}\*"
   
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\${UNINSTALLNAME}.exe"
  
  ;!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    	
	;Create shortcuts
    CreateDirectory "$SMPROGRAMS\${PROJECTNAME}"
    CreateShortCut "$SMPROGRAMS\${PROJECTNAME}\${UNINSTALLNAME}.lnk" "$INSTDIR\${UNINSTALLNAME}.exe"
    CreateShortCut "$SMPROGRAMS\${PROJECTNAME}\${EXENAMENICE}.lnk" "$INSTDIR\${EXENAME}.exe"
  
  ;!insertmacro MUI_STARTMENU_WRITE_END
  
    ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
  IntFmt $0 "0x%08X" $0
  WriteRegDWORD HKLM "${ARP}" "EstimatedSize" "$0"

  ExecWait "$INSTDIR\vcredist_x86.exe"

  MessageBox MB_OK "Opossum Tournament Manager ${VERSION} successfully installed."
  Quit
  
SectionEnd
 
# create a section to define what the uninstaller does.
# the section will always be named "Uninstall"
Section "Uninstall"
 
# Always delete uninstaller first
Delete $INSTDIR\${UNINSTALLNAME}.exe

Delete "$SMPROGRAMS\${PROJECTNAME}\${UNINSTALLNAME}.lnk"
Delete "$SMPROGRAMS\${PROJECTNAME}\${EXENAMENICE}.lnk"
RMDir "$SMPROGRAMS\${PROJECTNAME}"
 
# now delete installed file
RMDir /r "$INSTDIR"
 
SectionEnd