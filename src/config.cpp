#include "config.h"

Config * Config::m_instance = Q_NULLPTR;

Config &Config::instance()
{
    if(m_instance == Q_NULLPTR)
        m_instance = new Config();

    return *m_instance;
}

Config::Config() :
    m_winnerPoints(1),
    m_loserPoints(0),
    m_drawPoints(0),
    m_defaultFinalists(8),
    m_defaultRingsCount(3),
    m_winnerBgColor("#BBFF73"),
    m_loserBgColor("#FF6B69"),
//    _drawBgColor("#FF6B69"),
//    _drawBgColor("#FFF58A"),
    m_drawBgColor(Qt::transparent),
    m_winnerFontColor(Qt::black),
    m_loserFontColor(Qt::white),
//    _drawFontColor(Qt::white),
    m_drawFontColor(Qt::black),
    m_finalsMode(FinalsMode::Olympic)
{

}

Config &config()
{
    return Config::instance();
}
