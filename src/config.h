#ifndef CONFIG_H
#define CONFIG_H

#include <QColor>

enum FinalsMode {
    Olympic,
    Full
};

class Config
{
public:
    static Config& instance();
    const int winnerPoints() { return m_winnerPoints; }
    const int loserPoints() { return m_loserPoints; }
    const int drawPoints() { return m_drawPoints; }

    const int defaultFinalists() {return m_defaultFinalists; }
    const int defaultRingsCount() {return m_defaultRingsCount; }

    const QColor winnerBgColor() { return m_winnerBgColor; }
    const QColor loserBgColor() { return m_loserBgColor; }
    const QColor drawBgColor() { return m_drawBgColor; }

    const QColor winnerFontColor() { return m_winnerFontColor; }
    const QColor loserFontColor() { return m_loserFontColor; }
    const QColor drawFontColor() { return m_drawFontColor; }

    const FinalsMode finalsMode() { return m_finalsMode; }
private:
    explicit Config();
    int m_winnerPoints;
    int m_loserPoints;
    int m_drawPoints;

    int m_defaultFinalists;
    int m_defaultRingsCount;

    QColor m_winnerBgColor;
    QColor m_loserBgColor;
    QColor m_drawBgColor;
    QColor m_winnerFontColor;
    QColor m_loserFontColor;
    QColor m_drawFontColor;

    FinalsMode m_finalsMode;

    static Config *m_instance;
};

Config& config();

#endif // CONFIG_H
