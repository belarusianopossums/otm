#include "duel.h"
#include "fighter.h"

Duel::Duel()
{
    m_first = Q_NULLPTR;
    m_second = Q_NULLPTR;
}

Duel::Duel(Fighter *first, Fighter *second)
{
    setFighters(first, second);
}

Duel::~Duel()
{

}

void Duel::setFighters(Fighter *first, Fighter *second)
{
    m_first = first;
    m_second = second;
    setFirstFighterId(first->id());
    setSecondFighterId(second->id());
}

int Duel::firstFighterId() const
{
    return m_firstFighterId;
}

void Duel::setFirstFighterId(const int firstFighterId)
{
    m_firstFighterId = firstFighterId;
}

int Duel::secondFighterId() const
{
    return m_secondFighterId;
}

void Duel::setSecondFighterId(const int secondFighterId)
{
    m_secondFighterId = secondFighterId;
}

QDataStream &operator<<(QDataStream &out, Duel *duel)
{
    out << duel->firstFighterId()
        << duel->secondFighterId()
        << duel->id()
        << duel->round()
        << duel->ring();
    qDebug() << "Writing duel " << duel->firstFighter()->id() << " vs " << duel->secondFighter()->id();

    return out;
}

QDataStream &operator>>(QDataStream &in, Duel *&duel)
{
    duel = new Duel();
    int value;
    in >> value;
    duel->setFirstFighterId(value);
    in >> value;
    duel->setSecondFighterId(value);
    in >> value;
    duel->setId(value);
    in >> value;
    duel->setRound(value);
    in >> value;
    duel->setRing(value);
    qDebug() << "Reading duel " << duel->firstFighterId() << " vs " << duel->secondFighterId();

    return in;
}
