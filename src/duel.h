#ifndef DUEL_H
#define DUEL_H

#include "fighter.h"

#include <QDataStream>
#include <QDebug>

class Duel
{
public:
    Duel();
    Duel(Fighter *first, Fighter *second);
    ~Duel();

    Fighter * firstFighter() const { return m_first; }
    Fighter * secondFighter() const { return m_second; }

    void setFighters(Fighter *first, Fighter *second);

    int id() const { return m_id;}
    void setId(const int id) { m_id = id; }

    int round() const { return m_round;}
    void setRound(const int round) { m_round = round; }

    int ring() const { return m_ring;}
    void setRing(const int ring) { m_ring = ring; }

    int firstFighterId() const;
    void setFirstFighterId(const int firstFighterId);

    int secondFighterId() const;
    void setSecondFighterId(const int secondFighterId);
private:
    Fighter *m_first;
    Fighter *m_second;
    int m_firstFighterId;
    int m_secondFighterId;
    int m_id;
    int m_round;
    int m_ring;
};

QDataStream &operator<<(QDataStream &out, Duel *duel);
QDataStream &operator>>(QDataStream &in, Duel *&duel);

#endif // DUEL_H
