#include "duelslist.h"
#include "ui_duelslist.h"

#include "duel.h"
#include "fighter.h"
#include "tournamentcontroller.h"
#include "scoredialog.h"
#include "config.h"

#include <QDebug>
#include <QTextCodec>

DuelsList::DuelsList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DuelsList)
{
    ui->setupUi(this);
}

DuelsList::~DuelsList()
{
    delete ui;
}

void DuelsList::setDuels(const QVector<Duel *> duels)
{
    m_duels = duels;
    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(duels.count());
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << tr("Боец 1") << tr("Счёт")
                                              << tr("Боец 2") << tr("Счёт"));
    TournamentController *controller = TournamentController::instance();
    int row = 0;
    foreach(Duel *d, duels) {
        QTableWidgetItem *firstFighterItem = new QTableWidgetItem(d->firstFighter()->name());
        QTableWidgetItem *secondFighterItem = new QTableWidgetItem(d->secondFighter()->name());
        QTableWidgetItem *firstFighterScoreItem_f1 = new QTableWidgetItem("0");
        QTableWidgetItem *secondFighterScoreItem_f1 = new QTableWidgetItem("0");
        ui->tableWidget->setItem(d->id(), 0, firstFighterItem);
        ui->tableWidget->setItem(d->id(), 1, firstFighterScoreItem_f1);
        ui->tableWidget->setItem(d->id(), 2, secondFighterItem);
        ui->tableWidget->setItem(d->id(), 3, secondFighterScoreItem_f1);
        updateScoreUI(row, controller->scoreInDuel(d->firstFighter(), d->secondFighter()), controller->scoreInDuel(d->secondFighter(), d->firstFighter()));
        row++;
    }
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

QString DuelsList::toHTML(const QString &header, bool convertToUtf)
{
    QString html = htmlHeader(header, false) + htmlTable(header, false) + htmlFooter(false);

    if(convertToUtf) {
        return QTextCodec::codecForLocale()->toUnicode(html.toUtf8());
    }
    else {
        return html;
    }
}

QString DuelsList::htmlHeader(const QString &header, bool convertToUtf)
{
    QString html = "<html><head><meta charset=\"utf-8\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/><title>" + header + "</title></head><body>";

    if(convertToUtf) {
        return QTextCodec::codecForLocale()->toUnicode(html.toUtf8());
    }
    else {
        return html;
    }
}

QString DuelsList::htmlFooter(bool convertToUtf)
{
    QString html = "</body></html>";

    if(convertToUtf) {
        return QTextCodec::codecForLocale()->toUnicode(html.toUtf8());
    }
    else {
        return html;
    }
}

QString DuelsList::htmlTable(const QString &header, bool convertToUtf)
{
    QString html;
    QStringList fighters;
    QStringList fightersList;
    int i = 0;
    foreach(Duel *d, m_duels) {
        if(!fighters.contains(d->firstFighter()->name())) {
            fightersList.append(QString("%1. %2").arg(++i).arg(d->firstFighter()->name()));
            fighters.append(d->firstFighter()->name());
        }
        if(!fighters.contains(d->secondFighter()->name())) {
            fightersList.append(QString("%1. %2").arg(++i).arg(d->secondFighter()->name()));
            fighters.append(d->secondFighter()->name());
        }
    }
    html += "<div><table border=\"1\" cellpadding=\"10\" cellspacing=\"0\">";
    html += "<tr><th colspan=\"5\" align=\"center\"><b>" + header + "</b></th><th align=\"center\"><b>Список бойцов</b></th></tr>";
    html += "<tr><td>№</td><td>Боец 1</td><td> Счёт </td><td>Боец 2</td><td> Счёт </td><td rowspan=\"" +
            QString::number(m_duels.count()) + "\">" + fightersList.join("<br />") + "</td></tr>";
    foreach(Duel *d, m_duels) {
        html += "<tr><td>" + QString::number(d->id() + 1) + "</td><td>" + d->firstFighter()->name() + "</td><td>&nbsp;</td><td>" + d->secondFighter()->name() + "</td><td>&nbsp;</td></tr>";
    }
    html += "</table></div>";

    if(convertToUtf) {
        return QTextCodec::codecForLocale()->toUnicode(html.toUtf8());
    }
    else {
        return html;
    }
}

void DuelsList::on_tableWidget_cellDoubleClicked(int row, int column)
{
    Q_UNUSED(column)
    TournamentController *controller = TournamentController::instance();

    ScoreDialog *scoreDialog = new ScoreDialog(this);
    int initialScores[4];
    initialScores[0] = controller->scoreInDuel(m_duels[row]->firstFighter(), m_duels[row]->secondFighter());
    initialScores[1] = controller->scoreInDuel(m_duels[row]->secondFighter(), m_duels[row]->firstFighter());
    scoreDialog->setData(m_duels[row]->firstFighter()->name(),
                         initialScores[0],
                         m_duels[row]->secondFighter()->name(),
                         initialScores[1]);
    scoreDialog->exec();
    if(scoreDialog->applyChanges()) {
        applyScore(scoreDialog, row, initialScores);
        emit scoreChangedForFighter(m_duels[row]->firstFighter()->id());
        emit scoreChangedForFighter(m_duels[row]->secondFighter()->id());
    }

    delete scoreDialog;
}

QVector<Duel *> DuelsList::duels() const
{
    return m_duels;
}

void DuelsList::applyScore(const ScoreDialog *scoreDialog, int row, int *initialScores)
{
    Fighter *winner = Q_NULLPTR;
    Fighter *loser = Q_NULLPTR;

    int firstFighterScore;
    int secondFighterScore;

    int initialFirstFighterScore = initialScores[0];
    int initialSecondFighterScore = initialScores[1];

    TournamentController *controller = TournamentController::instance();

    firstFighterScore = scoreDialog->firstFighterScore_f1();
    secondFighterScore = scoreDialog->secondFighterScore_f1();

    if(firstFighterScore == 0 && secondFighterScore == 0)
        return;

    if(firstFighterScore > secondFighterScore){
        winner = m_duels[row]->firstFighter();
        loser = m_duels[row]->secondFighter();
    }
    else {
        winner = m_duels[row]->secondFighter();
        loser = m_duels[row]->firstFighter();
    }

    // Apply scores
    controller->setScoreInDuel(m_duels[row]->firstFighter(), m_duels[row]->secondFighter(), firstFighterScore, secondFighterScore);

    int initialDelta = initialFirstFighterScore - initialSecondFighterScore;
    int delta = firstFighterScore - secondFighterScore;

    int correctPointsFirst = 0;
    int correctPointsSecond = 0;
    if(initialDelta == 0 && initialFirstFighterScore > 0) {
        correctPointsFirst = config().drawPoints();
        correctPointsSecond = config().drawPoints();
    }
    if(initialDelta > 0) {
        correctPointsFirst = config().winnerPoints();
        correctPointsSecond = config().loserPoints();
    }
    if(initialDelta < 0) {
        correctPointsFirst = config().loserPoints();
        correctPointsSecond = config().winnerPoints();
    }

    m_duels[row]->firstFighter()->adjustRating(delta - initialDelta);
    m_duels[row]->secondFighter()->adjustRating(-delta + initialDelta);

    m_duels[row]->firstFighter()->addTotalScore(-correctPointsFirst);
    m_duels[row]->secondFighter()->addTotalScore(-correctPointsSecond);

    if(m_mode == Finals) {
        m_duels[row]->firstFighter()->addFinalsScore(-correctPointsFirst);
        m_duels[row]->secondFighter()->addFinalsScore(-correctPointsSecond);
    }

    Config conf = config();
    if(firstFighterScore == secondFighterScore) {
        winner->addTotalScore(config().drawPoints());
        loser->addTotalScore(config().drawPoints());
    } else {
        winner->addTotalScore(conf.winnerPoints());
        loser->addTotalScore(conf.loserPoints());
    }

    if(m_mode == Finals) {
        if(firstFighterScore == secondFighterScore) {
            winner->addFinalsScore(config().drawPoints());
            loser->addFinalsScore(config().drawPoints());
        } else {
            winner->addFinalsScore(config().winnerPoints());
            loser->addFinalsScore(config().loserPoints());
        }
    }

    // Update  UI
    updateScoreUI(row, firstFighterScore, secondFighterScore);
}

void DuelsList::updateScoreUI(int row, int firstFighterScore, int secondFighterScore)
{
    if(firstFighterScore == secondFighterScore) {
        ui->tableWidget->item(row, 0)->setBackgroundColor(config().drawBgColor());
        ui->tableWidget->item(row, 0)->setTextColor(config().drawFontColor());
        ui->tableWidget->item(row, 1)->setBackgroundColor(config().drawBgColor());
        ui->tableWidget->item(row, 1)->setTextColor(config().drawFontColor());
        ui->tableWidget->item(row, 2)->setBackgroundColor(config().drawBgColor());
        ui->tableWidget->item(row, 2)->setTextColor(config().drawFontColor());
        ui->tableWidget->item(row, 3)->setBackgroundColor(config().drawBgColor());
        ui->tableWidget->item(row, 3)->setTextColor(config().drawFontColor());
    }
    else if(firstFighterScore > secondFighterScore) {
        ui->tableWidget->item(row, 0)->setBackgroundColor(config().winnerBgColor());
        ui->tableWidget->item(row, 0)->setTextColor(config().winnerFontColor());
        ui->tableWidget->item(row, 1)->setBackgroundColor(config().winnerBgColor());
        ui->tableWidget->item(row, 1)->setTextColor(config().winnerFontColor());
        ui->tableWidget->item(row, 2)->setBackgroundColor(config().loserBgColor());
        ui->tableWidget->item(row, 2)->setTextColor(config().loserFontColor());
        ui->tableWidget->item(row, 3)->setBackgroundColor(config().loserBgColor());
        ui->tableWidget->item(row, 3)->setTextColor(config().loserFontColor());
    }
    else {
        ui->tableWidget->item(row, 0)->setBackgroundColor(config().loserBgColor());
        ui->tableWidget->item(row, 0)->setTextColor(config().loserFontColor());
        ui->tableWidget->item(row, 1)->setBackgroundColor(config().loserBgColor());
        ui->tableWidget->item(row, 1)->setTextColor(config().loserFontColor());
        ui->tableWidget->item(row, 2)->setBackgroundColor(config().winnerBgColor());
        ui->tableWidget->item(row, 2)->setTextColor(config().winnerFontColor());
        ui->tableWidget->item(row, 3)->setBackgroundColor(config().winnerBgColor());
        ui->tableWidget->item(row, 3)->setTextColor(config().winnerFontColor());
    }
    ui->tableWidget->item(row, 1)->setText(QString::number(firstFighterScore));
    ui->tableWidget->item(row, 3)->setText(QString::number(secondFighterScore));

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

FightersListMode DuelsList::mode() const
{
    return m_mode;
}

void DuelsList::setMode(const FightersListMode &mode)
{
    m_mode = mode;
}

