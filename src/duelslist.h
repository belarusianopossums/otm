#ifndef DUELSLIST_H
#define DUELSLIST_H

#include <QWidget>

#include "fighter.h"

namespace Ui {
class DuelsList;
}

class Duel;
class ScoreDialog;

class DuelsList : public QWidget
{
    Q_OBJECT

public:
    explicit DuelsList(QWidget *parent = 0);
    ~DuelsList();

    QVector<Duel *> duels() const;
    void setDuels(const QVector<Duel *> duels);
    QString toHTML(const QString &header, bool convertToUtf = true);

    static QString htmlHeader(const QString &header, bool convertToUtf = true);
    static QString htmlFooter(bool convertToUtf = true);
    QString htmlTable(const QString &header, bool convertToUtf = true);

    FightersListMode mode() const;
    void setMode(const FightersListMode &mode);

signals:
    void scoreChanged();
    void scoreChangedForFighter(int id);

private slots:
    void on_tableWidget_cellDoubleClicked(int row, int column);

private:
    Ui::DuelsList *ui;
    QVector<Duel *> m_duels;
    FightersListMode m_mode;

    void applyScore(const ScoreDialog *scoreDialog, int row, int *initialScores);
    void updateScoreUI(int row, int firstFighterScore, int secondFighterScore);
};

#endif // DUELSLIST_H
