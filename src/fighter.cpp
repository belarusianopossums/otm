#include "fighter.h"
#include "tournamentcontroller.h"

Fighter::Fighter(QString name)
{
    setName(name);
    setStatus(Active);
    m_id = 0;
    m_groupsScore = 0;
    m_finalsScore = 0;
    m_totalScore = 0;
    m_rating = 0;
}

Fighter::~Fighter()
{

}

void Fighter::setGroupScore(const int groupScore)
{
    m_groupsScore = groupScore;
    emit scoreChanged(this, Groups);
}

void Fighter::increaseGroupScore(const int scoreDelta)
{
    m_groupsScore += scoreDelta;
    emit scoreChanged(this, Groups);
}

void Fighter::decreaseGroupScore(const int scoreDelta)
{
    m_groupsScore -= scoreDelta;
    emit scoreChanged(this, Groups);
}

void Fighter::setFinalsScore(const int finalsScore)
{
    m_finalsScore += finalsScore;
    emit scoreChanged(this, Groups);
}

void Fighter::increaseFinalsScore(const int scoreDelta)
{
    m_finalsScore += scoreDelta;
    emit scoreChanged(this, Finals);
}

void Fighter::decreaseFinalsScore(const int scoreDelta)
{
    m_finalsScore -= scoreDelta;
    emit scoreChanged(this, Finals);
}

int Fighter::score(FightersListMode mode) const
{
    switch(mode) {
        case Groups:
            //return groupsScore();
            return rating();
        case Finals:
            return finalsScore();
    }
    return 0;
}

void Fighter::increaseScore(const FightersListMode mode)
{
    switch(mode) {
        case Groups:
            increaseGroupScore();
        case Finals:
            increaseFinalsScore();
    }
}

void Fighter::decreaseScore(const FightersListMode mode)
{
    switch(mode) {
        case Groups:
            decreaseGroupScore();
        case Finals:
            decreaseFinalsScore();
    }
}

QString Fighter::statusString() const
{
    switch(m_status) {
    case Active:
        return "В строю";
    case Off:
        return "Отсеян";
    case Resigned:
        return "Выбыл";
    }
    return "";
}

void Fighter::setRingRivals(const QVector<Fighter *> ringRivals)
{
    m_ringRivals.clear();
    foreach(Fighter * f, ringRivals) {
        if(m_rivals.contains(f))
            m_ringRivals.append(f);
    }
}

void Fighter::removeRival(Fighter *rival)
{
    m_rivals.removeAll(rival);
    m_ringRivals.removeAll(rival);
}

void Fighter::resetLocalData()
{
    m_ringRivals.clear();
    m_idle = 0;
}

int Fighter::calculateRating()
{
    return m_rating;
}

int Fighter::rating() const
{
    return m_rating;
}

void Fighter::setRating(const int rating)
{
    m_rating = rating;
}

void Fighter::adjustRating(const int delta)
{
    m_rating += delta;
    emit ratingChanged(this);
}
int Fighter::totalScore() const
{
    return m_totalScore;
}

void Fighter::setTotalScore(const int totalScore)
{
    m_totalScore = totalScore;
}

void Fighter::addTotalScore(const int score)
{
    m_totalScore += score;
}

void Fighter::addFinalsScore(const int score)
{
    m_finalsScore += score;
}

QVector<Fighter *> Fighter::rivals() const
{
    return m_rivals;
}

QVector<int> Fighter::rivalsIds() const
{
    QVector<int> result;
    foreach(Fighter * rival, m_rivals) {
        result.append(rival->id());
    }
    return result;
}

void Fighter::setRivals(const QVector<int> rivals)
{
    m_rivals.clear();
    foreach(int id, rivals) {
        Fighter *fighter = TournamentController::instance()->fighterById(id);
        if(fighter) {
            m_rivals.append(fighter);
        }
    }
}

void Fighter::setRivals(const QVector<Fighter *> rivals)
{
    m_rivals = rivals;
}

QVector<Fighter *> Fighter::ringRivals() const
{
    return m_ringRivals;
}

QVector<int> Fighter::ringRivalsIds() const
{
    QVector<int> result;
    foreach(Fighter * rival, m_ringRivals) {
        result.append(rival->id());
    }
    return result;
}

void Fighter::setRingRivals(const QVector<int> rivals)
{
    m_ringRivals.clear();
    foreach(int id, rivals) {
        Fighter *fighter = TournamentController::instance()->fighterById(id);
        if(fighter) {
            m_ringRivals.append(fighter);
        }
    }
}

void Fighter::removeRingRival(Fighter *rival)
{
    m_ringRivals.removeAll(rival);
}

QVector<Fighter *> Fighter::rivalsDuelled() const
{
    return m_rivalsDuelled;
}

QVector<int> Fighter::rivalsDuelledIds() const
{
    QVector<int> result;
    foreach(Fighter * rival, m_rivalsDuelled) {
        result.append(rival->id());
    }
    return result;
}

void Fighter::setRivalsDuelled(const QVector<int> rivals)
{
    m_rivalsDuelled.clear();
    foreach(int id, rivals) {
        Fighter *fighter = TournamentController::instance()->fighterById(id);
        if(fighter) {
            m_rivalsDuelled.append(fighter);
        }
    }
}

int Fighter::idle() const
{
    return m_idle;
}

void Fighter::setIdle(const int idle)
{
    m_idle = idle;
}

void Fighter::increaseIdle()
{
    m_idle++;
}



void writeToStream(QDataStream &out, QVector<int> &vint)
{
    out << vint.count();
    qDebug() << "Writing " << vint.count() << " int values";
    foreach(int value, vint) {
        out << value;
    }
}
