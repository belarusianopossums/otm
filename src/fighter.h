#ifndef FIGHTER_H
#define FIGHTER_H

#include <QVector>
#include <QString>
#include <QObject>
#include <QDataStream>
#include <QDebug>

enum FighterStatus {
    Active,
    Off,
    Resigned
};

enum FightersListMode {
    Groups,
    Finals
};

class Fighter: public QObject
{
    Q_OBJECT
public:
    Fighter(QString name = QString());
    ~Fighter();

    int id() const { return m_id; }
    void setId(const int id) { m_id = id; }

    QString name() const { return m_name; }
    void setName(const QString &name) { m_name = name; }

    int group() const { return m_group; }
    void setGroup(const int group) { m_group = group; }

    int groupsScore() const { return m_groupsScore; }
    void setGroupScore(const int groupScore);
    void increaseGroupScore(const int scoreDelta = 1);
    void decreaseGroupScore(const int scoreDelta = 1);

    int finalsScore() const { return m_finalsScore; }
    void setFinalsScore(const int finalsScore);
    void increaseFinalsScore(const int scoreDelta = 1);
    void decreaseFinalsScore(const int scoreDelta = 1);

    int score(FightersListMode mode) const ;
    void increaseScore(const FightersListMode mode);
    void decreaseScore(const FightersListMode mode);

    FighterStatus status() const { return m_status; }
    void setStatus(const FighterStatus status) { m_status = status; }
    QString statusString() const;

    void resetLocalData();

    int calculateRating();
    int rating() const;
    void setRating(const int rating = 0);
    void adjustRating(const int delta);

    int totalScore() const;
    void setTotalScore(const int totalScore);

    void addTotalScore(const int score);
    void addFinalsScore(const int score);

    QVector<Fighter *> rivals() const;
    QVector<int> rivalsIds() const ;
    void setRivals(const QVector<int> rivals);
    void setRivals(const QVector<Fighter *> rivals);
    void removeRival(Fighter *rival);

    QVector<Fighter *> ringRivals() const;
    QVector<int> ringRivalsIds() const;
    void setRingRivals(const QVector<Fighter *> ringRivals);
    void setRingRivals(const QVector<int> rivals);
    void removeRingRival(Fighter *rival);

    QVector<Fighter *> rivalsDuelled() const;
    QVector<int> rivalsDuelledIds() const;
    void setRivalsDuelled(const QVector<int> rivals);

    int idle() const;
    void setIdle(const int idle);
    void increaseIdle();

signals:
    void scoreChanged(Fighter *fighter, FightersListMode mode);
    void ratingChanged(Fighter *fighter);

private:
    int m_id;
    QString m_name;
    int m_groupsScore;
    int m_finalsScore;
    int m_totalScore;
    int m_group;
    int m_rating;
    int m_idle;
    FighterStatus m_status;
    QVector<Fighter *> m_rivals;
    QVector<Fighter *> m_ringRivals;
    QVector<Fighter *> m_rivalsDuelled;
};

 void writeToStream(QDataStream &out, QVector<int> &vint);

inline QDataStream &operator>>(QDataStream &in, QVector<int> &vint) {
    vint.clear();
    int size;
    in >> size;
    qDebug() << "Reading " << size << " int values";
    vint.reserve(size);
    for(int i = 0; i < size; i++) {
        int value;
        in >> value;
        vint.append(value);
    }
    return in;
}

inline QDataStream &operator<<(QDataStream &out, const Fighter &fighter)
{
    out << fighter.id()
        << fighter.name().toUtf8()
        << fighter.groupsScore()
        << fighter.finalsScore()
        << fighter.totalScore()
        << fighter.group()
        << fighter.rating()
        << (int)fighter.status()
        << fighter.idle();

    /*writeToStream(out, fighter.rivalsIds());
    writeToStream(out, fighter.ringRivalsIds());
    writeToStream(out, fighter.rivalsDuelledIds());*/
/*
    out << fighter.rivalsIds().count();
    qDebug() << "Writing " << fighter.rivalsIds().count() << " int values";
    foreach(int value, fighter.rivalsIds()) {
        out << value;
    }

    out << fighter.ringRivalsIds().count();
    qDebug() << "Writing " << fighter.ringRivalsIds().count() << " int values";
    foreach(int value, fighter.ringRivalsIds()) {
        out << value;
    }

    out << fighter.rivalsDuelledIds().count();
    qDebug() << "Writing " << fighter.rivalsDuelledIds().count() << " int values";
    foreach(int value, fighter.rivalsDuelledIds()) {
        out << value;
    }
*/
    return out;
}

inline QDataStream &operator>>(QDataStream &in, Fighter &fighter)
{
    int id;
    in >> id;
    fighter.setId(id);
    qDebug() << "Id:" << fighter.id();

    QByteArray name;
    in >> name;
    fighter.setName(QString::fromUtf8(name));
    qDebug() << "Name:" << fighter.name();

    int groupScore;
    in >> groupScore;
    fighter.setGroupScore(groupScore);
    qDebug() << "Group score:" << fighter.groupsScore();

    int finalsScore;
    in >> finalsScore;
    fighter.setFinalsScore(finalsScore);
    qDebug() << "Finals score:" << fighter.finalsScore();

    int totalScore;
    in >> totalScore;
    fighter.setTotalScore(totalScore);
    qDebug() << "Total score:" << fighter.totalScore();

    int group;
    in >> group;
    fighter.setGroup(group);
    qDebug() << "Group:" << fighter.group();

    int rating;
    in >> rating;
    fighter.setRating(rating);
    qDebug() << "Rating:" << fighter.rating();

    int status;
    in >> status;
    fighter.setStatus((FighterStatus)status);
    qDebug() << "Status:" << fighter.status();

    int idle;
    in >> idle;
    fighter.setIdle(idle);
    qDebug() << "Idle:" << fighter.idle();
/*
    QVector<int> rivals;
    in >> rivals;
    fighter.setRivals(rivals);

    QVector<int> ringRivals;
    in >> ringRivals;
    fighter.setRivals(ringRivals);

    QVector<int> rivalsDuelled;
    in >> rivalsDuelled;
    fighter.setRivals(rivalsDuelled);
*/
    return in;
}

#endif // FIGHTER_H
