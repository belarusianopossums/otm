#include "fighterslist.h"
#include "ui_fighterslist.h"
#include "tournamentcontroller.h"
#include "stylemanager.h"
#include "utils.h"

#include <QSortFilterProxyModel>
#include <QMessageBox>

FightersList::FightersList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FightersList)
{
    ui->setupUi(this);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

FightersList::~FightersList()
{
    delete ui;
}

void FightersList::setModel(QAbstractItemModel *model)
{
    ui->tableView->setModel(model);
    connect(model, SIGNAL(rowsInserted(QModelIndex,int,int)), this, SLOT(updateData()));
    connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(updateData()));
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void FightersList::updateData()
{
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void FightersList::on_tableView_doubleClicked(const QModelIndex &index)
{
    Fighter * fighter = TournamentController::instance()->model()->fighter(qobject_cast<QSortFilterProxyModel *>(ui->tableView->model())->mapToSource(index));
    if(fighter->status() == FighterStatus::Resigned) {
        return;
    }

    int ret =  showStandardQuestion(tr("Подтверждение"), QString("Вы уверены, что хотите дезактивировать бойца %1?").arg(fighter->name()));

    if(ret == QMessageBox::Rejected)
        return;

    TournamentController::instance()->resignFighter(fighter);
}

