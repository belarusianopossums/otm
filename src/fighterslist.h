#ifndef FIGHTERSLIST_H
#define FIGHTERSLIST_H

#include <QWidget>
#include <QVector>

#include "fighter.h"

class QAbstractItemModel;

namespace Ui {
class FightersList;
}

class FightersList : public QWidget
{
    Q_OBJECT

public:
    explicit FightersList(QWidget *parent = 0);
    ~FightersList();

    void setMode(const FightersListMode mode) { m_mode = mode; }
    void setModel(QAbstractItemModel *model);

private slots:
    void on_tableView_doubleClicked(const QModelIndex &index);
    void updateData();

private:
    Ui::FightersList *ui;
    FightersListMode m_mode;
    QVector<Fighter *> m_fighters;
};

#endif // FIGHTERSLIST_H
