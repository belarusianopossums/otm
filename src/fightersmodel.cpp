#include "fightersmodel.h"

FightersModel::FightersModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    m_headers.append(tr("Имя"));
    m_headers.append(tr("Статус"));
    //_headers.append(tr("Рейтинг"));
    m_headers.append(tr("Общий счёт"));
    m_headers.append(tr("Счёт в финалах"));
}

FightersModel::~FightersModel()
{
    clearData();
}

/**
 * @brief Overridden method for row count check.
 * @param parent index of parent.
 * @return row count.
 */
int FightersModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_data.size();
}

/**
 * @brief Overridden method for column count check.
 * @param parent index of parent.
 * @return column count.
 */
int FightersModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_headers.size();
}

/**
 * @brief Overridden method returning data used for column header.
 * @param section column.
 * @param orientation orientation (vertical or horizontal).
 * @param role role of returned data.
 * @return data.
 */
QVariant FightersModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole &&
       orientation == Qt::Horizontal)
    {
        return m_headers.at(section);
    }
    else
    {
        return QVariant();
    }
}

Qt::ItemFlags FightersModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index)
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

void FightersModel::clearData()
{
    if( rowCount() > 0 )
    {
        beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
        qDeleteAll(m_data);
        m_data.clear();
        endRemoveRows();
    }
}

QVector<Fighter *> FightersModel::fighters() const
{
    return m_data;
}

void FightersModel::addFighter(Fighter *fighter)
{
    beginInsertRows(QModelIndex(), m_data.size(), m_data.size());
    m_data.append(fighter);
    endInsertRows();
}

void FightersModel::addFighters(const QVector<Fighter *> fighters)
{
    beginInsertRows(QModelIndex(), m_data.size(), m_data.size() + fighters.count() - 1);
    foreach(Fighter *fighter, fighters)
        m_data.append(fighter);
    endInsertRows();
}

void FightersModel::setFighterData(const int id, const Fighter *data)
{
    if(id >= m_data.count())
        return;

    m_data[id]->setName(data->name());
    m_data[id]->setGroup(data->group());
    //_data[id]->setRating(data->rating());
    m_data[id]->setTotalScore(data->totalScore());
    m_data[id]->setFinalsScore(data->finalsScore());
    m_data[id]->setStatus(data->status());

    fighterDataChanged(id);
}

void FightersModel::fighterDataChanged(const int id)
{
    emit dataChanged(index(id, 0), index(id, m_headers.count() - 1));
}

void FightersModel::fighterDataChanged()
{
    emit dataChanged(index(0, 0), index(m_data.count() - 1, m_headers.count() - 1));
}

Fighter *FightersModel::fighter(const QModelIndex &index)
{
    return m_data[index.row()];
}

int FightersModel::resignedFighters()
{
    int result = 0;
    foreach(Fighter *f, m_data) {
        if(f->status() == Resigned) {
            result++;
        }
    }
    return result;
}

