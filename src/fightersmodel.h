#ifndef FIGHTERSMODEL_H
#define FIGHTERSMODEL_H

#include "fighter.h"

#include <QAbstractTableModel>

class FightersModel : public QAbstractTableModel
{
public:
    FightersModel(QObject *parent = Q_NULLPTR);
    ~FightersModel();

    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    /**
     * @brief Overridden method returning data from model for given index.
     * @param indexToRetrieve index used to retrieve data.
     * @param role role of data.
     * @return data.
     */
    virtual inline QVariant data(const QModelIndex& indexToRetrieve,
                                 int role) const
    {
        if( indexToRetrieve.isValid() && Qt::DisplayRole == role )
        {
            const Fighter* fighter = m_data.at(indexToRetrieve.row());
            switch(indexToRetrieve.column()) {
            case 0: return fighter->name();
            case 1: return fighter->statusString();
            //case 2: return fighter->rating();
            case 2: return fighter->totalScore();
            case 3: return fighter->finalsScore();
            }
            return QVariant();
        }
        else
        {
            return QVariant();
        }
    }

    virtual QVariant headerData(int section,
                                Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const override;

    virtual Qt::ItemFlags flags(const QModelIndex& index) const override;
    void clearData();

    QVector<Fighter *> fighters() const;
    void addFighter(Fighter *fighter);
    void addFighters(const QVector<Fighter *> fighters);

    void setFighterData(const int id, const Fighter *data);
    void fighterDataChanged(const int id);
    void fighterDataChanged();

    Fighter *fighter(const QModelIndex &index);
    int resignedFighters();

private:
    Q_DISABLE_COPY(FightersModel)

    QVector<Fighter *> m_data;
    QVector<QString> m_headers;
};

#endif // FIGHTERSMODEL_H
