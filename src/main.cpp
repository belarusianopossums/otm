#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setAttribute(Qt::AA_EnableHighDpiScaling);
    a.setApplicationName(QObject::tr("Opossum Tournament Manager"));
    a.setApplicationVersion("1.65");

    MainWindow w;
    w.showMaximized();

    return a.exec();
}
