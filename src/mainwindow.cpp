#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "duelslist.h"
#include "config.h"
#include "stylemanager.h"
#include "utils.h"

#include <QFileDialog>
#include <QTabWidget>
#include <QDebug>
#include <QMessageBox>
#include <QCloseEvent>
#include <QSortFilterProxyModel>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintPreviewDialog>
#include <QPainter>
#include <QTextDocument>
#include <QTextCursor>
#include <QTextBlockFormat>
#include <QImage>
#include <QDataStream>
#include <QWindow>
#include <QFontMetrics>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(QString("%1 %2").arg(qApp->applicationName(), qApp->applicationVersion()));

    ui->splitter->setSizes(QList<int>() << 100 << 100);

    controller = TournamentController::instance();

    connect(controller, &TournamentController::saveFileOpened, this, &MainWindow::saveFileOpened);
    connect(controller, &TournamentController::autosaveFileExists, this, &MainWindow::autosaveFileExists);

    controller->init();

    ui->rings->setMinimum(2);
    ui->rings->setMaximum(3);
    ui->rings->setValue(config().defaultRingsCount());
    controller->setRingsCount(config().defaultRingsCount());

    ui->iteration->setMinimum(1);
    ui->iteration->setMaximum(controller->groupCombinations.count());
    ui->iteration->setValue(1);

    ui->finalFighters->setValue(config().defaultFinalists());

    ui->fighters->setMode(Groups);

    QSortFilterProxyModel* proxyModel = new QSortFilterProxyModel();
    proxyModel->setSourceModel(controller->model());
    ui->fighters->setModel(proxyModel);

    ui->exportPath->setText(controller->exportPath());

    updateState();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(controller->status() == INITIAL)
        return;

    int ret = showStandardQuestion(tr("Выход"), tr("Вы действительно хотите выйти? Данные турнира автоматически сохранены."));

    if(ret == QDialog::Accepted) {
        event->accept();
    } else {
        event->ignore();
    }
}
void MainWindow::on_loadButton_clicked()
{
    loadFighters();
}

void MainWindow::on_makeGroupsButton_clicked()
{
    createGroups();
}

void MainWindow::on_exportGroupsButton_clicked()
{
    exportGroups();
}

void MainWindow::on_makeFinalsButton_clicked()
{
    createFinals();
}

void MainWindow::on_exportFinalsButton_clicked()
{
    exportFinals();
}

void MainWindow::on_buttonChangeExportPath_clicked()
{
    QString newPath = QFileDialog::getExistingDirectory(this, "Путь экспорта", controller->exportPath());
    if(!newPath.isEmpty()) {
        controller->setExportPath(newPath);
        ui->exportPath->setText(newPath);
    }
}

void MainWindow::on_rings_valueChanged(const QString &arg1)
{
    // Set rings count
    controller->setRingsCount(arg1.toInt());
    qDebug() << "Rings count set: " << controller->ringsCount();
    ui->iteration->setMinimum(1);
    ui->iteration->setMaximum(controller->groupCombinations.count());
}

void MainWindow::clearDuelsUI(const int startIteration)
{
    int tabsCount = ui->prepFights->count();
    for(int i = startIteration; i < tabsCount; i++) {
        QTabWidget *tab = static_cast<QTabWidget *>(ui->prepFights->widget(startIteration));
        ui->prepFights->removeTab(startIteration);
        delete tab;
    }
    m_tabIDs.clear();
}

void MainWindow::loadFighters()
{
    // Load fighters list
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Открыть список бойцов"), controller->exportPath(), tr("Список бойцов (*.txt *.list)"));
    if(fileName.isEmpty())
        return;

    TournamentController::instance()->readFighters(fileName);
    ui->finalFighters->setMaximum(controller->fighters().count());
    clearDuelsUI(0);

    updateState();
}

void MainWindow::openFile()
{
    QString openFile = QFileDialog::getOpenFileName(this,
        tr("Открыть файл"),
        controller->exportPath(),
        tr("*.otm"));
    if(openFile.isEmpty()) {
        return;
    }

    controller->readSaveFile(openFile);
}

void MainWindow::saveFile()
{
    if(m_saveFile.isEmpty())
    m_saveFile = QFileDialog::getSaveFileName(this,
        tr("Сохранить в файл"),
        controller->exportPath(),
        tr("*.otm"));
    if(!m_saveFile.endsWith(".otm"))
        m_saveFile = m_saveFile + ".otm";

    controller->writeSaveFile(m_saveFile);
}

void MainWindow::createGroups()
{
    int startIteration = ui->iteration->text().toInt() - 1;
    if(startIteration < 0 || startIteration >= controller->groupCombinations.count())
        return;

    if(controller->status() != FIGHTERS_LOADED) {
        int ret = showStandardQuestion(tr("Подтверждение"), tr("Вы уверены, что хотите сформировать группы? Предыдущие группы будут стёрты!"));
        if(ret == QDialog::Rejected) {
            return;
        }
   }

    // Prepare groups
    controller->makeGroups();
    controller->setFightersRivals();

    m_maxIterations = controller->groupCombinations.count();

    QVector<QVector<QVector<Duel *>>> groupsDuels = controller->groupsDuels();
    for(int i = startIteration; i < controller->groupCombinations.count(); i++) {
        QVector<QVector<Duel *>> duels = controller->makeDuelsList(i);
        if(groupsDuels.count() <= i) {
            groupsDuels.append(duels);
        }
        else {
            groupsDuels[i] = duels;
        }
    }
    controller->setGroupsDuels(groupsDuels);
    createGroupsUI(startIteration);
    controller->setStatus(GROUPS_GENERATED);
    updateState();
}

void MainWindow::exportGroups()
{
    if(controller->status() < GROUPS_GENERATED)
        return;

    QString path = controller->exportPath();
    QDir dir(path);
    dir.setNameFilters(QStringList() << "*.html");
    dir.setFilter(QDir::Files);
    foreach(QString dirFile, dir.entryList())
    {
        dir.remove(dirFile);
    }

    for(int iteration = 0; iteration < m_maxIterations; iteration++) {
        QTabWidget *tabWidget = static_cast<QTabWidget *>(ui->prepFights->widget(iteration));
        int maxRings = controller->ringsCount();
        for(int ring = 0; ring < maxRings; ring++) {
            DuelsList * duelsList = static_cast<DuelsList *>(tabWidget->widget(ring));
            QString exportFile = path + "\\round" + QString::number(iteration + 1) + "-ring-" + QString::number(ring + 1) + ".html";
            QString html = duelsList->toHTML("Заход " + QString::number(iteration + 1) + ", ристалище " + QString::number(ring + 1));

            QFile file(exportFile);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                break;
            QTextStream out(&file);
            out << html;
            file.close();
        }
    }
    QMessageBox::information(this, tr("Группы"), tr("Экспорт групп завершен"));
}

void MainWindow::createFinals()
{
    if(controller->status() < GROUPS_GENERATED)
        return;

    int finalistsCount = ui->finalFighters->text().toInt();
    if(config().finalsMode() == Olympic) {
    qreal l2 = log2(finalistsCount);
        if((l2 - (long)l2) != 0) {
            QMessageBox::information(this, tr("Ошибка"), tr("Количество финалистов должно быть степенью двух в олимпийской системе (2, 4, 8, 16)!"));
            return;
        }
    }

    int ret = showStandardQuestion(tr("Подтверждение"), tr("Точно запустить финал? Вернуть все обратно не получится, перепроверьте данные дважды!"));

    if(ret == QDialog::Rejected) {
        return;
    }

    QVector<Duel *> finals = controller->makeFinals(finalistsCount, false);
    qDebug() << "Final duels: " << finals.count();
    controller->setFinals(finals);
    createFinalsUI();
    controller->setStatus(FINALS_GENERATED);

    QMessageBox::information(this, tr("Финалы"), tr("Финалы сгенерированы"));
    updateState();
}

void MainWindow::exportFinals()
{
    if(controller->status() < FINALS_GENERATED)
        return;

    QString path = controller->exportPath();

    if(config().finalsMode() == Full) {
        DuelsList * duelsList = static_cast<DuelsList *>(ui->prepFights->widget(ui->prepFights->count() - 1));
        QString exportFile = path + "/finals.html";
        QString html = duelsList->toHTML("Финалы");

        QFile file(exportFile);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;
        QTextStream out(&file);
        out << html;
        file.close();
    }
    if(config().finalsMode() == Olympic) {
        if(m_finalsImage.isNull()) {
            createFinalsImage(QSize(1600, 1200));
        }
        m_finalsImage.save(path + "/finals.jpg");
    }

    QMessageBox::information(this, "Финалы", "Экспорт финалов завершен");
}

void MainWindow::printGroups()
{
    QPrinter printer;

     QPrintPreviewDialog dialog(&printer, this);
     connect(&dialog, SIGNAL(paintRequested(QPrinter*)), this, SLOT(paintRequestedForGroups(QPrinter*)));
     dialog.setWindowTitle(tr("Распечатать группы"));
     if (dialog.exec() != QDialog::Accepted) {
         return;
     }
}

void MainWindow::printFinals()
{
    QPrinter printer;

     QPrintPreviewDialog dialog(&printer, this);
     connect(&dialog, SIGNAL(paintRequested(QPrinter*)), this, SLOT(paintRequestedForFinals(QPrinter*)));
     dialog.setWindowTitle(tr("Распечатать финалы"));
     if (dialog.exec() != QDialog::Accepted) {
         return;
     }
}

void MainWindow::createGroupsUI(const int startIteration)
{
    clearDuelsUI(startIteration);

    QVector<QVector<QVector<Duel *>>> groupsDuels = controller->groupsDuels();
    for(int i = startIteration; i < controller->groupCombinations.count(); i++) {
        QTabWidget * prepTabWidget = new QTabWidget();
        m_tabIDs.append(ui->prepFights->addTab(prepTabWidget, "Заход " + QString::number(i + 1)));
        for(int ring = 0; ring < controller->ringsCount(); ring++) {
            DuelsList * duelsList = new DuelsList();
            duelsList->setMode(Groups);
            duelsList->setDuels(groupsDuels[i][ring]);
            prepTabWidget->addTab(duelsList, "Ристалище " + QString::number(ring + 1));
            connect(duelsList, SIGNAL(scoreChanged()), controller, SLOT(scoreChanged()));
            connect(duelsList, SIGNAL(scoreChangedForFighter(int)), controller, SLOT(scoreChanged(int)));
        }
    }
    qDebug() << ui->prepFights->count() << " tabs exist after groups creation";
}

void MainWindow::createFinalsUI()
{
    if(config().finalsMode() == Full) {
        DuelsList *finalDuels = new DuelsList();
        finalDuels->setMode(Finals);
        finalDuels->setDuels(controller->finals());
        m_tabIDs.append(ui->prepFights->addTab(finalDuels, "Финалы"));
    }
}

void MainWindow::showGroups()
{
    QWidget *widget = new QWidget();
    QSize widgetSize(300, 400);
    widget->setGeometry(QRect(QPoint(this->x(), this->y()) + this->rect().center() - QPoint(widgetSize.width()/2, widgetSize.height()/2), widgetSize));
    QLabel* label = new QLabel(widget);
    widget->setWindowModality(Qt::WindowModal);
    label->setTextFormat(Qt::RichText);

    QString html = "<table cellpadding=\"10\">";
    for(int i = 0; i < controller->groupsCount() / 2; i++) {
        html += "<tr>";
        {
            int group = i * 2;
            QStringList names;
            foreach(Fighter *fighter, controller->fightersInGroup(group)) {
                names.append(fighter->name());
            }
            html += "<td><div align=\"center\"><b>Группа " + QString::number(group + 1) + "</b></div>" + names.join("<br />") + "</td>";
        }
        {
            int group = i * 2 + 1;
            QStringList names;
            foreach(Fighter *fighter, controller->fightersInGroup(group)) {
                names.append(fighter->name());
            }
            html += "<td><div align=\"center\"><b>Группа " + QString::number(group + 1) + "</b></div>" + names.join("<br />") + "</td>";
        }
        html += "</tr>";
    }
    html += "</table";
    label->setText(html);
    widget->show();
}

void MainWindow::createFinalsImage(const QSize docSize)
{
    int finalistsCount = ui->finalFighters->text().toInt();
    QList<int> finals{0,1};
    int lastUsed = 1;
    int steps = log2(finalistsCount) - 1;
    for(int i = 1; i <= steps; i++) {
        for(int pos = 0; pos < pow(2, i); pos++) {
            finals.insert(pos * 2 + 1, ++lastUsed);
        }
    }
    qDebug() << finals;
    int shiftX = 100;
    int shiftY = 10;
    int maxSteps = steps + 2;
    QSize rectSize((docSize.width() - 20 - shiftX * (maxSteps * 2)) / (maxSteps), docSize.height() / finalistsCount - shiftY * 2);
    m_finalsImage = QImage(docSize, QImage::Format_RGB32);
    QPainter painter(&m_finalsImage);
    QPen pen;
    pen.setColor(Qt::black);
    pen.setWidth(4);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);
    painter.setPen(pen);
    painter.fillRect(0, 0, docSize.width(), docSize.height(), Qt::white);

    QFont font("Arial");
    int minFontSize = 1000;
    for(int i = 0; i < finalistsCount; i++) {
        qreal fontSize = 4;
        int textWidth = 0;
        while(textWidth < (rectSize.width() - 10)) {
            fontSize += 0.5;
            font.setPointSizeF(fontSize);
            QFontMetrics metrics(font);
            QString name = controller->finalFighters()[i]->name();
            textWidth = metrics.width(name);
        }
        if(fontSize < minFontSize) {
            minFontSize = fontSize;
        }
    }
    font.setPointSizeF(minFontSize);
    painter.setFont(font);

    for(int i = 0; i < maxSteps; i++) {
        int itemsCount = pow(2, maxSteps - i - 1);
        for(int j = 0; j < itemsCount; j++) {
            // Draw rect
            shiftY = (docSize.height() / itemsCount - rectSize.height()) / 2;
            QRect rect(QPoint(10 + (rectSize.width() + shiftX) * i, shiftY + (shiftY * 2 + rectSize.height()) * j), rectSize);
            painter.drawRect(rect);
            if(i == 0) {
                // Draw text
                QString name = controller->finalFighters()[finals[j]]->name();
                QRect textRect = QFontMetrics(font).boundingRect(name);
                painter.drawText(rect.translated((rect.width() - textRect.width()) / 2, (rect.height() - textRect.height()) / 2), name);
            }
            else {
                // Draw lines
                int prevIndex = j * 2;
                int prevStep = i - 1;
                int prevItemsCount = pow(2, maxSteps - prevStep - 1);
                int prevShiftY = (docSize.height() / prevItemsCount - rectSize.height()) / 2;
                QRect prevRect1(QPoint(10 + (rectSize.width() + shiftX) * prevStep, prevShiftY + (prevShiftY * 2 + rectSize.height()) * prevIndex), rectSize);
                QRect prevRect2(QPoint(10 + (rectSize.width() + shiftX) * prevStep, prevShiftY + (prevShiftY * 2 + rectSize.height()) * (prevIndex + 1)), rectSize);
                painter.drawLine(prevRect1.right() + 1, prevRect1.center().y(), rect.left() - 1, rect.center().y());
                painter.drawLine(prevRect2.right() + 1, prevRect2.center().y(), rect.left() - 1, rect.center().y());
            }
        }
    }
}

void MainWindow::updateState()
{
    ui->actionLoadFighters->setEnabled(true);
    ui->actionOpenFile->setEnabled(true);
    ui->actionSaveFile->setEnabled(false);
    ui->actionExit->setEnabled(true);

    setButtonStyle(ui->loadButton, true);
    setButtonStyle(ui->buttonChangeExportPath, true);

    switch(controller->status()) {
    case INITIAL:
        ui->actionSaveFile->setEnabled(false);
        ui->actionCreateGroups->setEnabled(false);
        ui->actionExportGroups->setEnabled(false);
        ui->actionPrintGroups->setEnabled(false);
        ui->actionCreateFinals->setEnabled(false);
        ui->actionExportFinals->setEnabled(false);
        ui->actionPrintFinals->setEnabled(false);
        ui->actionShowGroups->setEnabled(false);

        setButtonStyle(ui->loadButton, true, true);
        setButtonStyle(ui->makeGroupsButton, false);
        setButtonStyle(ui->printGroupsButton, false);
        setButtonStyle(ui->makeFinalsButton, false);
        setButtonStyle(ui->printFinalsButton, false);

        break;
    case FIGHTERS_LOADED:
        ui->actionSaveFile->setEnabled(true);
        ui->actionCreateGroups->setEnabled(true);
        ui->actionExportGroups->setEnabled(false);
        ui->actionPrintGroups->setEnabled(false);
        ui->actionCreateFinals->setEnabled(false);
        ui->actionExportFinals->setEnabled(false);
        ui->actionPrintFinals->setEnabled(false);
        ui->actionShowGroups->setEnabled(false);

        setButtonStyle(ui->makeGroupsButton, true, true);
        setButtonStyle(ui->printGroupsButton, false);
        setButtonStyle(ui->makeFinalsButton, false);
        setButtonStyle(ui->printFinalsButton, false);

        break;
    case GROUPS_GENERATED:
        ui->actionSaveFile->setEnabled(true);
        ui->actionCreateGroups->setEnabled(true);
        ui->actionExportGroups->setEnabled(true);
        ui->actionPrintGroups->setEnabled(true);
        ui->actionCreateFinals->setEnabled(true);
        ui->actionExportFinals->setEnabled(false);
        ui->actionPrintFinals->setEnabled(false);
        ui->actionShowGroups->setEnabled(true);

        setButtonStyle(ui->makeGroupsButton, true);
        setButtonStyle(ui->printGroupsButton, true, true);
        setButtonStyle(ui->makeFinalsButton, true, true);
        setButtonStyle(ui->printFinalsButton, false);

        break;
    case FINALS_GENERATED:
        ui->actionSaveFile->setEnabled(true);
        ui->actionCreateGroups->setEnabled(false);
        ui->actionExportGroups->setEnabled(true);
        ui->actionPrintGroups->setEnabled(true);
        ui->actionCreateFinals->setEnabled(true);
        ui->actionExportFinals->setEnabled(true);
        ui->actionPrintFinals->setEnabled(true);
        ui->actionShowGroups->setEnabled(true);

        setButtonStyle(ui->makeGroupsButton, false);
        setButtonStyle(ui->printGroupsButton, true);
        setButtonStyle(ui->makeFinalsButton, true);
        setButtonStyle(ui->printFinalsButton, true, true);

        break;
    }
}

void MainWindow::setButtonStyle(QWidget *button, bool enabled, bool lastActive)
{
    button->setEnabled(enabled);
    StyleManager::instance().applyStyle(button, enabled ? (lastActive ? StyleManager::ButtonActive : StyleManager::ButtonEnabled) : StyleManager::ButtonDisabled);
}

void MainWindow::on_actionLoadFighters_triggered()
{
    loadFighters();
}

void MainWindow::on_actionSaveFile_triggered()
{
    saveFile();
}

void MainWindow::on_actionOpenFile_triggered()
{
    openFile();
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionCreateGroups_triggered()
{
    createGroups();
}

void MainWindow::on_actionExportGroups_triggered()
{
    exportGroups();
}

void MainWindow::on_actionCreateFinals_triggered()
{
    createFinals();
}

void MainWindow::on_actionExportFinals_triggered()
{
    exportFinals();
}

void MainWindow::on_actionPrintGroups_triggered()
{
    printGroups();
}

void MainWindow::on_actionPrintFinals_triggered()
{
    printFinals();
}

void MainWindow::paintRequestedForGroups(QPrinter *printer)
{
    QPainter painter;
    painter.begin(printer);
    for(int iteration = 0; iteration < m_maxIterations; iteration++) {
        QTabWidget *tabWidget = static_cast<QTabWidget *>(ui->prepFights->widget(iteration));
        int maxRings = controller->ringsCount();
        for(int ring = 0; ring < maxRings; ring++) {
            DuelsList * duelsList = static_cast<DuelsList *>(tabWidget->widget(ring));
            QString header = "Заход " + QString::number(iteration + 1) + ", ристалище " + QString::number(ring + 1);
            QString html = duelsList->htmlTable(header, false);
            QTextDocument doc;
            doc.setHtml(html);
            painter.save();
            qreal printerHeight = (qreal)printer->pageRect().height();
            qreal docHeight = (qreal)doc.size().height();
            qreal sy = printerHeight/docHeight;
            if(docHeight > printerHeight) {
                painter.setTransform(QTransform().scale(sy, sy));
            }
            doc.drawContents(&painter);
            painter.restore();
            printer->newPage();
        }
    }
}

void MainWindow::paintRequestedForFinals(QPrinter *printer)
{
    if(config().finalsMode() == Full) {
        DuelsList * duelsList = static_cast<DuelsList *>(ui->prepFights->widget(ui->prepFights->count() - 1));
        QString header = "Финалы";
        QString html = duelsList->htmlTable(header, false);
        QTextDocument doc;
        doc.setHtml(html);
        doc.print(printer);
    }
    if(config().finalsMode() == Olympic) {
        printer->setPageOrientation(QPageLayout::Landscape);
        QSize docSize(printer->pageRect().size().width() * 2, printer->pageRect().size().height() * 2);
        createFinalsImage(docSize);
        QPainter printPainter(printer);
        printPainter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);
        printPainter.setTransform(QTransform().scale(0.5, 0.5));
        printPainter.drawImage(0, 0, m_finalsImage);
    }
}

void MainWindow::on_printGroupsButton_clicked()
{
    printGroups();
}

void MainWindow::on_printFinalsButton_clicked()
{
    printFinals();
}

void MainWindow::on_actionShowGroups_triggered()
{
    showGroups();
}

void MainWindow::autosaveFileExists()
{
    int ret = showStandardQuestion(tr("Подтверждение"), tr("Обнаружен файл автосохранения. Загрузить его?"));

    controller->existingAutosaveFeedbackReceived(ret == QDialog::Accepted);
}

void MainWindow::saveFileOpened(QString errorString)
{
    if(!errorString.isEmpty()) {
        QMessageBox::critical(this, tr("Ошибка!"), errorString);
        return;
    }

    if(controller->status() >= GROUPS_GENERATED) {
        createGroupsUI();
    }
    if(controller->status() == FINALS_GENERATED) {
        createFinalsUI();
    }

    updateState();
}
