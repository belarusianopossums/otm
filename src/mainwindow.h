#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>

#include "tournamentcontroller.h"

class QPrinter;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    TournamentController *controller;
protected:
    void closeEvent(QCloseEvent *event);
private slots:
    void on_loadButton_clicked();

    void on_makeGroupsButton_clicked();

    void on_exportGroupsButton_clicked();

    void on_makeFinalsButton_clicked();

    void on_exportFinalsButton_clicked();

    void on_buttonChangeExportPath_clicked();

    void on_rings_valueChanged(const QString &arg1);

    void on_actionLoadFighters_triggered();

    void on_actionSaveFile_triggered();

    void on_actionOpenFile_triggered();

    void on_actionExit_triggered();

    void on_actionCreateGroups_triggered();

    void on_actionExportGroups_triggered();

    void on_actionCreateFinals_triggered();

    void on_actionExportFinals_triggered();

    void on_actionPrintGroups_triggered();

    void on_actionPrintFinals_triggered();

    void paintRequestedForGroups(QPrinter *printer);

    void paintRequestedForFinals(QPrinter *printer);

    void on_printGroupsButton_clicked();

    void on_printFinalsButton_clicked();

    void on_actionShowGroups_triggered();

    void autosaveFileExists();
    void saveFileOpened(QString errorString);

private:
    Ui::MainWindow *ui;
    QVector<int> m_tabIDs;
    int m_maxIterations;
    QString m_saveFile;
    QImage m_finalsImage;

    void clearDuelsUI(const int startIteration);

    void loadFighters();
    void openFile();
    void saveFile();
    void createGroups();
    void exportGroups();
    void createFinals();
    void exportFinals();
    void printGroups();
    void printFinals();

    void createGroupsUI(const int startIteration = 0);
    void createFinalsUI();

    void showGroups();
    void createFinalsImage(const QSize docSize);
    void updateState();

    void setButtonStyle(QWidget *button, bool enabled, bool lastActive = false);
};

#endif // MAINWINDOW_H
