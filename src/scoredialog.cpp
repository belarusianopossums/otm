#include "scoredialog.h"
#include "stylemanager.h"
#include "ui_scoredialog.h"

ScoreDialog::ScoreDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ScoreDialog)
{
    ui->setupUi(this);

    StyleManager styles = StyleManager::instance();
    styles.applyStyle(ui->okButton, StyleManager::ButtonOK);
    styles.applyStyle(ui->cancelButton, StyleManager::ButtonCancel);

    m_applyChanges = false;
}

ScoreDialog::~ScoreDialog()
{
    delete ui;
}

void ScoreDialog::setData(const QString &firstFighter, const int firstFighterScore_f1, const QString &secondFighter, const int secondFighterScore_f1)
{
    ui->firstFighterName_f1->setText(firstFighter);
    ui->secondFighterName_f1->setText(secondFighter);
    ui->firstFighterScore_f1->setText(QString::number(firstFighterScore_f1));
    ui->secondFighterScore_f1->setText(QString::number(secondFighterScore_f1));
}

bool ScoreDialog::applyChanges()
{
    return m_applyChanges;
}

int ScoreDialog::firstFighterScore_f1() const
{
    return ui->firstFighterScore_f1->text().toInt();
}

int ScoreDialog::secondFighterScore_f1() const
{
    return ui->secondFighterScore_f1->text().toInt();
}

void ScoreDialog::on_okButton_clicked()
{
    m_applyChanges = true;
    close();
}

void ScoreDialog::on_cancelButton_clicked()
{
    close();
}
