#ifndef SCOREDIALOG_H
#define SCOREDIALOG_H

#include <QDialog>

namespace Ui {
class ScoreDialog;
}

class ScoreDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ScoreDialog(QWidget *parent = 0);
    ~ScoreDialog();

    void setData(const QString &firstFighter, const int firstFighterScore_f1, const QString &secondFighter, const int secondFighterScore_f1);
    bool applyChanges();

    int firstFighterScore_f1() const;
    int secondFighterScore_f1() const;

private slots:
    void on_okButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::ScoreDialog *ui;
    bool m_applyChanges;
};

#endif // SCOREDIALOG_H
