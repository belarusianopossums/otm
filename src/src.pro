#-------------------------------------------------
#
# Project created by QtCreator 2015-04-17T09:25:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = OpossumTournamentManager
TEMPLATE = app

RC_ICONS = ..\res\opossum.ico

SOURCES += \
    main.cpp\
    mainwindow.cpp \
    duel.cpp \
    fighter.cpp \
    tournamentcontroller.cpp \
    fighterslist.cpp \
    duelslist.cpp \
    scoredialog.cpp \
    fightersmodel.cpp \
    config.cpp \
    tournamentstate.cpp \
    stylemanager.cpp \
    utils.cpp

HEADERS += \
    mainwindow.h \
    duel.h \
    fighter.h \
    tournamentcontroller.h \
    fighterslist.h \
    duelslist.h \
    scoredialog.h \
    fightersmodel.h \
    config.h \
    tournamentstate.h \
    stylemanager.h \
    utils.h

FORMS += \
    mainwindow.ui \
    fighterslist.ui \
    duelslist.ui \
    duelscoredialog.ui \
    scoredialog.ui

RESOURCES +=

*-g++ {
    QMAKE_CXXFLAGS+=-Wno-ignored-qualifiers
}
