#include "stylemanager.h"

#include <QWidget>

StyleManager::StyleManager()
{
    setStyle(ButtonEnabled, QStringLiteral("background-color:#0044b2; color:white; padding:10px; border: 0; min-width:100px;"));
    setStyle(ButtonDisabled, QStringLiteral("background-color:#b5b5b5; color:white; padding:10px; border: 0; min-width:100px;"));
    setStyle(ButtonActive, QStringLiteral("background-color:#077712; color:white; padding:10px; border: 0; min-width:100px;"));
    setStyle(ButtonOK, QStringLiteral("background-color:#077712; color:white; padding:10px; border: 0; min-width:100px;"));
    setStyle(ButtonCancel, QStringLiteral("background-color:#ba0000; color:white; padding:10px; border: 0; min-width:100px;"));

}

StyleManager &StyleManager::instance()
{
     static StyleManager s;
     return s;
}

QString StyleManager::style(Style style)
{
    return m_styles.value(style, QStringLiteral(""));
}

void StyleManager::setStyle(Style style, const QString &styleValue)
{
    m_styles[style] = styleValue;
}

bool StyleManager::applyStyle(QWidget *widget, Style styleName)
{
    QString style = this->style(styleName);
    if(style.isEmpty()) {
        return false;
    }

    widget->setStyleSheet(style);
    return true;
}
