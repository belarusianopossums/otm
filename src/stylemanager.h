#ifndef STYLEMANAGER_H
#define STYLEMANAGER_H

#include <QMap>
#include <QString>

class QWidget;

class StyleManager
{
public:
    enum Style {
        ButtonEnabled,
        ButtonDisabled,
        ButtonActive,
        ButtonOK,
        ButtonCancel
    };

    StyleManager();
    static StyleManager& instance();

    QString style(Style style);
    void setStyle(Style style, const QString &styleValue);

    bool applyStyle(QWidget *widget, Style styleName);
private:
    QMap<Style, QString> m_styles;
};

#endif // STYLEMANAGER_H
