#include "tournamentcontroller.h"
#include "config.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QApplication>
#include <QTextCodec>
#include <QFile>

const int saveFileHeaderSize = 4;

TournamentController * TournamentController::m_controller = Q_NULLPTR;

TournamentController::TournamentController()
{
}

QVector<QVector<QVector<Duel *> > > TournamentController::groupsDuels() const
{
    return m_state.groupsDuels;
}

void TournamentController::setGroupsDuels(const QVector<QVector<QVector<Duel *> > > &groupsDuels)
{
    m_state.groupsDuels = groupsDuels;
}

QByteArray TournamentController::stateToByteArray()
{
    return m_state.toByteArray();
}

void TournamentController::setStateFromByteArray(QByteArray &ba)
{
    m_state.fromByteArray(ba);
    m_fightersModel->clearData();
    m_fightersModel->addFighters(m_state.fighters);
}

QVector<Duel *> TournamentController::finals() const
{
    return m_state.finalsDuels;
}

void TournamentController::setFinals(const QVector<Duel *> &finals)
{
    m_state.finalsDuels = finals;
}

int TournamentController::groupsCount() const
{
    return m_state.groupsCount;
}

void TournamentController::setGroupsCount(int groupsCount)
{
    m_state.groupsCount = groupsCount;
}
TournamentStatus TournamentController::status() const
{
    return m_state.status;
}

void TournamentController::setStatus(const TournamentStatus &status)
{
    m_state.status = status;
    autosaveState();
}


QString TournamentController::exportPath() const
{
    return m_exportPath;
}

void TournamentController::setExportPath(const QString &exportPath)
{
    m_exportPath = exportPath;
    QString oldAutoSaveFile = m_autosaveFile;
    m_autosaveFile = m_exportPath + "/autosave.otm";
    if(m_autosaveFile != oldAutoSaveFile && QFile(m_autosaveFile).exists()) {
        emit autosaveFileExists();
    }
}


TournamentController::~TournamentController()
{
}

TournamentController *TournamentController::instance()
{
    if(!m_controller)
        m_controller = new TournamentController();
    return m_controller;
}

void TournamentController::init()
{
    m_fightersModel = new FightersModel();
    resetData();
    setExportPath(qApp->applicationDirPath());
}

void TournamentController::resetData()
{
    m_fightersModel->clearData();
    m_currentIteration = 0;
    m_state.resetState();
}

bool TournamentController::readFighters(const QString &filename)
{
    QFile file(filename);
    if(!file.exists()) return false;

    resetData();

    QVector<Fighter *> fighters;

    if(!file.open(QIODevice::ReadOnly)) return false;
    QTextStream in(&file);
    QByteArray line;
    while (!in.atEnd())
    {
       line = in.readLine().toUtf8();
       if(line.trimmed().isEmpty())
           continue;
       Fighter *fighter = new Fighter(line);
       fighter->setId(fighters.count());
       fighters.append(fighter);
    }
    file.close();

    m_state.setFighters(fighters);
    m_fightersModel->addFighters(fighters);

    if(fighters.count() > 0)
        setStatus(FIGHTERS_LOADED);

    autosaveState();

    return true;
}

QVector<Fighter *> TournamentController::fightersInGroup(int group) const
{
    QVector<Fighter *> result;
    foreach(Fighter *fighter, m_fightersModel->fighters()) {
        if(fighter->group() == group) {
            result.append(fighter);
        }
    }
    return result;
}

void TournamentController::setFighters(const QVector<Fighter *> fighters)
{
    m_fightersModel->clearData();
    m_fightersModel->addFighters(fighters);
}

Fighter *TournamentController::fighterById(int id) const
{
    foreach(Fighter * fighter, fighters()) {
        if(fighter->id() == id) {
            return fighter;
        }
    }

    return Q_NULLPTR;
}

void TournamentController::autosaveState()
{
    writeSaveFile(m_autosaveFile);
}

void TournamentController::setRingsCount(const int rings)
{
    if(rings <= 0) return;
    m_state.ringsCount = rings;
    m_state.groupsCount = m_state.ringsCount * 2;
    m_maxIteration = m_state.groupsCount - 1;

    groupCombinations = generateGroupsCombinations();
}

void TournamentController::makeGroups()
{
    int currentGroup = 0;
    foreach(Fighter *fighter, m_fightersModel->fighters()) {
        if(fighter->status() != Active)
            continue;

        fighter->setGroup(currentGroup);
        currentGroup++;
        if(currentGroup == m_state.groupsCount)
            currentGroup = 0;
    }
    autosaveState();
}

QVector<QVector<int>> TournamentController::generateGroupsCombinations() const
{
    QVector<QVector<int>> result;
    switch(m_state.ringsCount) {
    case 2:
        result.append(QVector<int>() << 0 << 1 << 2 << 3);
        result.append(QVector<int>() << 0 << 2 << 1 << 3);
        result.append(QVector<int>() << 0 << 3 << 1 << 2);
        break;
    case 3:
        result.append(QVector<int>() << 0 << 3 << 1 << 4 << 2 << 5);
        result.append(QVector<int>() << 0 << 4 << 1 << 5 << 2 << 3);
        result.append(QVector<int>() << 0 << 2 << 1 << 3 << 4 << 5);
        result.append(QVector<int>() << 0 << 1 << 2 << 4 << 3 << 5);
        result.append(QVector<int>() << 0 << 5 << 1 << 2 << 3 << 4);
        break;
    }

    return result;
}

void TournamentController::setFightersRivals()
{
    foreach(Fighter *fighter, m_fightersModel->fighters()) {
        QVector<Fighter *> rivals;
        foreach (Fighter * f, m_fightersModel->fighters()) {
            if(fighter != f && f->status() == Active && fighter->status() == Active)
                rivals.append(f);
        }
        fighter->setRivals(rivals);
    }
}

QVector<Duel *> TournamentController::makeFinals(const int finalistsCount, const bool generateDuels)
{
    QVector<Duel *> result;

    // Sort by fighters by score and get N best
    QVector<Fighter *> fighters = m_fightersModel->fighters();
    qSort(fighters.begin(), fighters.end(), [](Fighter *a, Fighter *b)->bool {
        if(a->totalScore() == b->totalScore()) {
            int aScore = TournamentController::instance()->scoreInDuel(a, b);
            int bScore = TournamentController::instance()->scoreInDuel(b, a);
            return aScore > bScore;
        }
        else
            return a->totalScore() > b->totalScore();
    });

    int i = 0;
    QVector<Fighter *> ringFighters;
    foreach(Fighter *f, fighters) {
        if(i <= finalistsCount && f->status() != Resigned) {
            ringFighters.append(f);
            f->setStatus(Active);
            i++;
        }
        if(i > finalistsCount && f->status() != Resigned) {
            f->setStatus(Off);
        }
    }
    m_state.finalFighters = ringFighters;

    if(!generateDuels) {
        return result;
    }

    // Make duel pairs
    Fighter * firstFighter;
    Fighter * secondFighter;

    setFightersRivals();
    foreach(Fighter *fighter, ringFighters) {
        fighter->resetLocalData();
        fighter->setRingRivals(ringFighters);
    }
    int duelsCount = ringFighters.count() * (ringFighters.count() - 1) / 2;
    for(int d = 0; d < duelsCount; d++) {
        firstFighter = selectLeastTiredFighterWithMostRivals(ringFighters);
        if(firstFighter == Q_NULLPTR)
            break;
        secondFighter = selectLeastTiredFighterWithMostRivals(firstFighter->ringRivals());
        foreach(Fighter * f, ringFighters)
            f->increaseIdle();
        firstFighter->setIdle(0);
        secondFighter->setIdle(0);
        firstFighter->removeRival(secondFighter);
        secondFighter->removeRival(firstFighter);
        Duel *duel = new Duel(firstFighter, secondFighter);
        result.append(duel);
        duel->setId(result.count() - 1);
    }

    for(int i = 0; i < m_fightersModel->fighters().count() * m_fightersModel->fighters().count(); i++) {
        //duels[i] = false;
        m_state.duelsScores[i] = 0;
    }

    autosaveState();
    return result;
}

QVector<QVector<Duel *>> TournamentController::makeDuelsList(const int round)
{
    QVector<QVector<Duel *>> result;

    if(round >= (m_state.groupsCount - 1))
        return result;

    QVector<int> groupCombination = groupCombinations[round];
    for(int ring = 0; ring < m_state.ringsCount; ring++) {
        QVector<Duel *> ringDuels;
        QVector<Fighter *> ringFighters;
        Fighter * firstFighter;
        Fighter * secondFighter;
        foreach(Fighter * fighter, m_fightersModel->fighters()) {
            if(fighter->status() == Active &&
               (fighter->group() == groupCombination[ring * 2] ||
               fighter->group() == groupCombination[ring * 2 + 1]))
                    ringFighters.append(fighter);
        }
        // Init fighters
        foreach(Fighter *fighter, ringFighters) {
            fighter->resetLocalData();
            fighter->setRingRivals(ringFighters);
        }
        int duelsCount = ringFighters.count() * (ringFighters.count() - 1) / 2;
        for(int d = 0; d < duelsCount; d++) {
            firstFighter = selectLeastTiredFighterWithMostRivals(ringFighters);
            if(firstFighter == Q_NULLPTR)
                break;
            secondFighter = selectLeastTiredFighterWithMostRivals(firstFighter->ringRivals());
            foreach(Fighter * f, ringFighters)
                f->increaseIdle();
            firstFighter->setIdle(0);
            secondFighter->setIdle(0);
            firstFighter->removeRival(secondFighter);
            secondFighter->removeRival(firstFighter);
            Duel *duel = new Duel(firstFighter, secondFighter);
            duel->setRing(ring);
            duel->setRound(round);
            ringDuels.append(duel);
            duel->setId(ringDuels.count() - 1);
        }
        result.append(ringDuels);
    }

    autosaveState();
    return result;
}

void TournamentController::shuffleGroups()
{
    if(m_currentIteration >= m_maxIteration) return;

    m_currentIteration++;
    makeDuelsList(m_currentIteration);
}

QVector<bool> TournamentController::fighterGroupStatistics(const Fighter *fighter)
{
    Q_UNUSED(fighter)
    QVector<bool> result;
    return result;
}

void TournamentController::resolveDuel(const Duel *duel, const Fighter *winner)
{
    int winnerIndex;
    int loserIndex;

    if(duel->firstFighter() == winner) {
        winnerIndex = duel->firstFighter()->id() * m_fightersModel->fighters().count() + duel->secondFighter()->id();
        loserIndex = duel->secondFighter()->id() * m_fightersModel->fighters().count() + duel->firstFighter()->id();
    }
    else {
        winnerIndex = duel->secondFighter()->id() * m_fightersModel->fighters().count() + duel->firstFighter()->id();
        loserIndex = duel->firstFighter()->id() * m_fightersModel->fighters().count() + duel->secondFighter()->id();
    }
    m_state.resultsInGroup[winnerIndex] = true;
    m_state.resultsInGroup[loserIndex] = false;

    autosaveState();
}

void TournamentController::resignFighter(Fighter *fighter)
{
    // Revert ratings/scores
    foreach(Fighter *f, m_fightersModel->fighters()) {
        f->removeRival(fighter);
        int initialResignedFighterScore = m_state.duelsScores[fighter->id() * m_fightersModel->fighters().count() + f->id()];
        int initialOtherFighterScore = m_state.duelsScores[f->id() * m_fightersModel->fighters().count() + fighter->id()];
        if(initialResignedFighterScore > 0 || initialOtherFighterScore > 0) {
            int oldDelta = qAbs(initialResignedFighterScore - initialOtherFighterScore);
            if(initialResignedFighterScore == initialOtherFighterScore) {
                f->addTotalScore(-config().drawPoints());
            }
            else if(initialResignedFighterScore > initialOtherFighterScore) {
                // Resigned fighter won
                //fighter->adjustRating(oldDelta);
                f->adjustRating(oldDelta);
                f->addTotalScore(-config().loserPoints());
            }
            else {
                // Resigned fighter lost
                f->adjustRating(-oldDelta);
                //fighter->adjustRating(oldDelta);
                f->addTotalScore(-config().winnerPoints());
            }
        }
    }

    fighter->setStatus(Resigned);
    //fighter->setGroupScore(-1);
    fighter->setTotalScore(-1000);
    fighter->setFinalsScore(-1000);
    fighter->setRating(-1000);
    m_fightersModel->fighterDataChanged();

    autosaveState();
}

void TournamentController::setFighterStatus(Fighter *fighter, FighterStatus status)
{
    fighter->setStatus(status);
    m_fightersModel->fighterDataChanged(fighter->id());
}

bool TournamentController::duelResult(const Duel *duel) const
{
    return duelResult(duel->firstFighter(), duel->secondFighter());
}

bool TournamentController::duelResult(const Fighter *firstFighter, const Fighter *secondFighter) const
{
    return m_state.resultsInGroup[firstFighter->id() * m_fightersModel->fighters().count() + secondFighter->id()];
}

Fighter *TournamentController::selectLeastTiredFighterWithMostRivals(const QVector<Fighter *> fighters) const
{
    int maxWeight = -1;
    Fighter * result = Q_NULLPTR;

    foreach(Fighter *f, fighters) {
        int maxRivalIdle = -1;
        int weight = 0;
        foreach(Fighter * rival, f->ringRivals()) {
            if(rival->idle() > maxRivalIdle) {
                maxRivalIdle = rival->idle();
            }
        }
        weight = f->ringRivals().count() * f->idle() * maxRivalIdle;
        if(weight > maxWeight) {
            result = f;
            maxWeight = weight;
        }
    }

    return (result && result->ringRivals().count() > 0) ?
                result :
                Q_NULLPTR;
}

FightersModel *TournamentController::model()
{
    return m_fightersModel;
}

bool TournamentController::winnerInDuel(const Fighter *first, const Fighter *second)
{
    return scoreInDuel(first, second) > scoreInDuel(second, first);
}

int TournamentController::scoreInDuel(const Fighter *first, const Fighter *second) const
{
    int index = first->id() * m_fightersModel->fighters().count() + second->id();
    return m_state.duelsScores[index];
}

void TournamentController::setScoreInDuel(Fighter *first, Fighter *second, int firstScore, int secondScore)
{
    m_state.duelsScores[(first->id() * m_fightersModel->fighters().count() + second->id())] = firstScore;
    m_state.duelsScores[(second->id() * m_fightersModel->fighters().count() + first->id())] = secondScore;
    autosaveState();
}

void TournamentController::scoreChanged()
{
    m_fightersModel->fighterDataChanged();
}

void TournamentController::scoreChanged(int id)
{
    m_fightersModel->fighterDataChanged(id);
}

void TournamentController::existingAutosaveFeedbackReceived(bool reuse)
{
    if(reuse) {
        readSaveFile(m_autosaveFile);
    }
}

void TournamentController::readSaveFile(const QString &saveFile)
{
    qDebug() << "Opening file " << saveFile;
    QFile file(saveFile);
    bool res = file.open(QIODevice::ReadOnly);
    if(!res) {
        return;
    }

    // Check header
    char header[saveFileHeaderSize];
    int version = header[3];
    file.read(header, saveFileHeaderSize);
    if(header[0] != 'O' ||
       header[1] != 'T' ||
       header[2] != 'M' ||
            version > 2) {
        file.close();
        emit saveFileOpened(tr("Неверный формат файла!"));
        return;
    }

    QByteArray data = file.readAll();
    resetData();
    setStateFromByteArray(data);
    groupCombinations = generateGroupsCombinations();

    file.close();
    emit saveFileOpened(QStringLiteral(""));
}

void TournamentController::writeSaveFile(const QString &saveFile)
{
    qDebug() << "Writing file " << saveFile;
    QFile file(saveFile);
    file.open(QIODevice::WriteOnly);

    char header[saveFileHeaderSize];
    header[0] = 'O';
    header[1] = 'T';
    header[2] = 'M';
    header[3] = 1;

    file.write(header, saveFileHeaderSize);
    file.write(stateToByteArray());
    file.close();

    emit saveFileWritten();
}
