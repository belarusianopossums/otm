#ifndef TOURNAMENTCONTROLLER_H
#define TOURNAMENTCONTROLLER_H

#include "fightersmodel.h"
#include "tournamentstate.h"

#include <QVector>

class TournamentController: public QObject
{
    Q_OBJECT
public:
    ~TournamentController();
    static TournamentController * instance();

    QVector<QVector<int>> groupCombinations;

    void init();
    void resetData();

    bool readFighters(const QString &filename);
    QVector<Fighter *> fighters() const { return m_state.fighters; }
    QVector<Fighter *> fightersInGroup(int group) const;
    void setFighters(const QVector<Fighter *>fighters);
    Fighter *fighterById(int id) const;
    void autosaveState();

    int ringsCount() const { return m_state.ringsCount; }
    void setRingsCount(const int rings);

    int iteration() const { return m_currentIteration; }

    // Assigns fighters to groups
    void makeGroups();
    QVector<QVector<int>> generateGroupsCombinations() const;
    void setFightersRivals();

    QVector<Duel *> makeFinals(const int finalistsCount, const bool generateDuels);

    // Returns vector of duels for each ring, combined into another vector of _ringCount size.
    QVector<QVector<Duel *>> makeDuelsList(const int round);
    void shuffleGroups();

    // Returns results of fighter's duels with all other fighters in groups.
    // Returned vector has the same size as _fighters
    QVector<bool> fighterGroupStatistics(const Fighter * fighter);

    void resolveDuel(const Duel *duel, const Fighter *winner);
    void resignFighter(Fighter *fighter);
    void setFighterStatus(Fighter *fighter, FighterStatus status);

    bool duelResult(const Duel * duel) const;
    bool duelResult(const Fighter * firstFighter, const Fighter * secondFighter) const;

    Fighter * selectLeastTiredFighterWithMostRivals(const QVector<Fighter *> fighters) const;

    FightersModel * model();

    bool winnerInDuel(const Fighter *first, const Fighter *second);

    int scoreInDuel(const Fighter *first, const Fighter *second) const;
    void setScoreInDuel(Fighter *first, Fighter *second, int firstScore, int secondScore);

    QString exportPath() const;
    void setExportPath(const QString &exportPath);

    TournamentStatus status() const;
    void setStatus(const TournamentStatus &status);

    int groupsCount() const;
    void setGroupsCount(int groupsCount);

    QVector<Fighter *> finalFighters() { return m_state.finalFighters; }

    QVector<Duel *> finals() const;
    void setFinals(const QVector<Duel *> &finals);

    QVector<QVector<QVector<Duel *> > > groupsDuels() const;
    void setGroupsDuels(const QVector<QVector<QVector<Duel *> > > &groupsDuels);

    QByteArray stateToByteArray();
    void setStateFromByteArray(QByteArray &ba);

public slots:
    void scoreChanged();
    void scoreChanged(int id);
    void existingAutosaveFeedbackReceived(bool reuse);
    void readSaveFile(const QString &saveFile);
    void writeSaveFile(const QString &saveFile);

signals:
    void autosaveFileExists();
    void saveFileOpened(QString);
    void saveFileWritten();

private:
    explicit TournamentController();

    static TournamentController * m_controller;

    int m_maxIteration;
    int m_currentIteration;
    FightersModel *m_fightersModel;
    QString m_exportPath;
    QString m_autosaveFile;

    TournamentState m_state;
};

#endif // TOURNAMENTCONTROLLER_H
