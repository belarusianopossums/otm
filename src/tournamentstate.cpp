#include "tournamentstate.h"

TournamentState::TournamentState()
{
    ringsCount = config().defaultRingsCount();
    groupsCount = 0;
    resultsInGroup = Q_NULLPTR;
    duelsScores = Q_NULLPTR;
    status = INITIAL;
}

void TournamentState::resetState()
{
    int duelsCount = fighters.count() * fighters.count();
    if(resultsInGroup)
        delete[] resultsInGroup;
    if(duelsScores)
        delete[] duelsScores;
    duelsScores = new int[duelsCount];
    resultsInGroup = new bool[duelsCount];
    memset(resultsInGroup, 0, sizeof(bool) * duelsCount);
    memset(duelsScores, 0, sizeof(int) * duelsCount);
}

void TournamentState::setFighters(const QVector<Fighter *> f)
{
    fighters = f;
    resetState();
}

QByteArray TournamentState::toByteArray()
{
    QByteArray ba;
    QDataStream data(&ba, QIODevice::WriteOnly);
    data << (int)status;

    data << ringsCount;
    data << groupsCount;

    int duelsCount = fighters.count() * fighters.count();
    qDebug() << "Writing " << duelsCount << " duels";
    data << duelsCount;
    data.writeRawData((char *)duelsScores, sizeof(duelsScores[0]) * duelsCount);
    data.writeRawData((char *)resultsInGroup, sizeof(resultsInGroup[0]) * duelsCount);

    data << fighters.count();
    for(const Fighter *fighter: qAsConst(fighters)) {
        data << *fighter;
    }
    data << finalFighters.count();
    for(const Fighter *fighter: qAsConst(finalFighters)) {
        data << fighter->id();
    }

   qDebug() << "Total byte array size: " << ba.size();
   data << finalsDuels;
   qDebug() << "Total byte array size: " << ba.size();
   data << groupsDuels;

   qDebug() << "Total byte array size: " << ba.size();
   return ba;
}

void TournamentState::fromByteArray(QByteArray &ba)
{
    QDataStream data(&ba, QIODevice::ReadOnly);

    int value;
    data >> value;
    status = (TournamentStatus)value;
    qDebug() << "Status: " << status;

    data >> ringsCount;
    qDebug() << "Rings: " << ringsCount;
    data >> groupsCount;
    qDebug() << "Groups: " << groupsCount;

    int duelsCount;
    data >> duelsCount;
    qDebug() << "Duels: " << duelsCount;
    if(duelsScores) delete[] duelsScores;
    if(resultsInGroup) delete[] resultsInGroup;
    duelsScores = new int[duelsCount];
    resultsInGroup = new bool[duelsCount];
    data.readRawData((char *)duelsScores, sizeof(duelsScores[0]) * duelsCount);
    data.readRawData((char *)resultsInGroup, sizeof(resultsInGroup[0]) * duelsCount);

    int fightersCount;
    data >> fightersCount;
    qDebug() << "Fighters: " << fightersCount;
    fighters.clear();
    for(int i = 0; i < fightersCount; i++) {
        Fighter * fighter = new Fighter();
        data >> *fighter;
        qDebug() << "Fighters: " << fighter->name();
        fighters.append(fighter);
    }
    data >> fightersCount;
    qDebug() << "Final fighters: " << fightersCount;
    finalFighters.clear();
    for(int i = 0; i < fightersCount; i++) {
        data >> value;
        Fighter *fighter = getFighterById(value);
        qDebug() << "Final fighter: " << fighter->name();
        if(fighter) finalFighters.append(fighter);
    }

    data >> finalsDuels;
    qDebug() << "Finals duels: " << finalsDuels.count();

    data >> groupsDuels;
    qDebug() << "Groups duels: " << groupsDuels.count();
    populateDuels();
}

Fighter *TournamentState::getFighterById(const int id) const
{
    foreach(Fighter *fighter, fighters) {
        if(fighter->id() == id) {
            return fighter;
        }
    }
    return Q_NULLPTR;
}

void TournamentState::populateDuels()
{
    for(Duel *duel: finalsDuels) {
        duel->setFighters(getFighterById(duel->firstFighterId()), getFighterById(duel->secondFighterId()));
    }
    for(auto rounds: groupsDuels) {
        for(auto rings: rounds) {
            for(Duel * duel: rings) {
                duel->setFighters(getFighterById(duel->firstFighterId()), getFighterById(duel->secondFighterId()));
            }
        }
    }
}
