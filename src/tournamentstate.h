#ifndef TOURNAMENTSTATE_H
#define TOURNAMENTSTATE_H

#include "fighter.h"
#include "duel.h"
#include "config.h"

#include <QVector>

enum TournamentStatus {
    INITIAL,
    FIGHTERS_LOADED,
    GROUPS_GENERATED,
    FINALS_GENERATED
};

class TournamentState
{
public:
    TournamentStatus status;

    int ringsCount;
    int groupsCount;

    bool *resultsInGroup;
    int *duelsScores;

    QVector<Fighter *> fighters;
    QVector<Fighter *> finalFighters;
    QVector<Duel *> finalsDuels;
    QVector<QVector<QVector<Duel *> > > groupsDuels; // [round][ring][duel]

    TournamentState();
    void resetState();
    void setFighters(const QVector<Fighter *>f);
    QByteArray toByteArray();
    void fromByteArray(QByteArray &ba);

    Fighter * getFighterById(const int id) const;
private:
    void populateDuels();
};

#endif // TOURNAMENTSTATE_H
