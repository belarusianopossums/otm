#include "stylemanager.h"

#include <QMessageBox>
#include <QPushButton>

int showStandardQuestion(const QString &title, const QString &text)
{
    QMessageBox msgBox(QMessageBox::Question, title, text);
    QAbstractButton* buttonYes = msgBox.addButton(QObject::tr("Да"), QMessageBox::AcceptRole);
    QAbstractButton* buttonNo = msgBox.addButton(QObject::tr("Нет"), QMessageBox::RejectRole);
    QObject::connect(buttonYes, &QAbstractButton::clicked, &msgBox, &QMessageBox::accept);
    QObject::connect(buttonNo, &QAbstractButton::clicked, &msgBox, &QMessageBox::reject);
    StyleManager::instance().applyStyle(buttonYes, StyleManager::ButtonOK);
    StyleManager::instance().applyStyle(buttonNo, StyleManager::ButtonCancel);
    return msgBox.exec();
}
