#ifndef UTILS_H
#define UTILS_H

#include <QString>

int showStandardQuestion(const QString &title, const QString &text);

#endif // UTILS_H
